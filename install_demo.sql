delete from `sys_options` WHERE `name` = 'bx_photos_uploader_switcher';
delete from `sys_options` WHERE `name` = 'bx_videos_uploader_switcher';
delete from `sys_options` WHERE `name` = 'bx_sounds_uploader_switcher';

SET @iKatID = (SELECT `ID` from `sys_options_cats` WHERE `name` = 'Photos' LIMIT 1);
INSERT INTO `sys_options` (`Name`, `VALUE`, `kateg`, `desc`, `Type`, `check`, `err_text`, `order_in_kateg`, `AvailableValues`)  VALUES
('bx_photos_uploader_switcher', 'html5,record,embed,sysfile', @iKatID, 'Available uploaders', 'list', '', '', 54, 'html5,regular,record,embed,sysfile');

SET @iKatID = (SELECT `ID` from `sys_options_cats` WHERE `name` = 'Videos' LIMIT 1);
INSERT INTO `sys_options` (`Name`, `VALUE`, `kateg`, `desc`, `Type`, `check`, `err_text`, `order_in_kateg`, `AvailableValues`)  VALUES
('bx_videos_uploader_switcher', 'html5,record,embed,sysfile', @iKatID, 'Available uploaders', 'list', '', '', 38, 'html5,regular,record,embed,sysfile');

SET @iKatID = (SELECT `ID` from `sys_options_cats` WHERE `name` = 'Sounds' LIMIT 1);
INSERT INTO `sys_options` (`Name`, `VALUE`, `kateg`, `desc`, `Type`, `check`, `err_text`, `order_in_kateg`, `AvailableValues`)  VALUES
('bx_sounds_uploader_switcher', 'html5,record,sysfile', @iKatID, 'Available uploaders', 'list', '', '', 38, 'html5,regular,record,sysfile');
